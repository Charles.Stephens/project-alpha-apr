from django import forms
from projects.models import Project


class AccountForm(forms.ModelForm):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = Project
        exclude = (
            "name",
            "owner",
            "description",
        )


class SignUpForm(forms.ModelForm):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = Project
        exclude = (
            "name",
            "owner",
            "description",
        )
